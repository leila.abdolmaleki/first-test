import React from 'react';
import ahan from './ahan.jpg';
import './card.css'

function Card(props) {
  return (

    <div className="card">
      <div className="img"
        style={{ backgroundImage: `url(${ahan})`, backgroundSize: 'cover' }}
      >
      </div>
      <div className="card-name">
        <h5>{props.name}</h5>
        <p>کارخونه :{props.factory}</p>
        <p>{props.length}</p>
        <button>جزئیات
          {/* <svg className="MuiSvgIcon-root MuiSvgIcon-fontSizeSmall" focusable="false" viewBox="0 0 80 80" aria-hidden="true">
          <path  d="H16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/>
          </svg> */}
          </button>
      
      </div>
      <div className="card-Price">
        <h4>قیمت برای هر کیلو <br/>
        <span>{props.price} تومان</span>
        </h4>
        <input
        placeholder="قیمت پیشنهادی شما" 
        type="number"
        />
      </div>
      <div className="card-button">
        <input className="card-input" placeholder="تناز وارد کنید" />
        <button className="card-add" >اضافه به سبد</button>
      </div>
    </div>
  )
}
export default Card;